# micromamba-tailscale

Docker image with both `micromamba` and `tailscale` support

## Configuration

Configure container behaviour using environment variables:

`TAILSCALE_AUTHKEY` is the authorization key used to register the container on
a Tailscale network.
`TAILSCALE_HOSTNAME` sets the name by which the docker container will be
reachable via the Tailscale network. If left blank, defaults to the container's hostname.
