# Check available MICROMAMBA_VERSION here: https://hub.docker.com/r/mambaorg/micromamba/tags
ARG UBUNTU_VERSION=noble
ARG MICROMAMBA_VERSION=2.0.5-ubuntu24.04

# Check available S6 verisons here: https://github.com/just-containers/s6-overlay/releases
ARG S6_OVERLAY_VERSION=3.2.0.2

FROM docker.io/mambaorg/micromamba:${MICROMAMBA_VERSION} AS micromamba

# Install apt packages
USER root

# Remove any third-party apt sources to avoid issues with expiring keys
RUN rm -f /etc/apt/sources.list.d/*.list

# For apt caching as per https://docs.docker.com/engine/reference/builder/#run---mounttypecache
RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

# Install curl
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  --mount=type=cache,target=/var/lib/apt,sharing=locked \
  apt update && apt-get --no-install-recommends install -y \
  curl \
  ca-certificates

# Add tailscale support and ssh client
RUN mkdir -p --mode=0755 /usr/share/keyrings
ARG UBUNTU_VERSION
RUN curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/${UBUNTU_VERSION}.noarmor.gpg > /usr/share/keyrings/tailscale-archive-keyring.gpg
RUN curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/${UBUNTU_VERSION}.tailscale-keyring.list > /etc/apt/sources.list.d/tailscale.list
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  --mount=type=cache,target=/var/lib/apt,sharing=locked \
  apt update && apt-get --no-install-recommends install -y \
  tailscale \
  openssh-client

# Add s6-overlay to run tailscaled service with proper process 1 / init supervision
# https://github.com/just-containers/s6-overlay
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
  --mount=type=cache,target=/var/lib/apt,sharing=locked \
  apt update && apt-get --no-install-recommends install -y \
  xz-utils
# Check available S6 verisons here: https://github.com/just-containers/s6-overlay/releases
ARG S6_OVERLAY_VERSION
RUN curl -sL https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-noarch.tar.xz | tar -C / -Jxp
RUN curl -sL https://github.com/just-containers/s6-overlay/releases/download/v${S6_OVERLAY_VERSION}/s6-overlay-x86_64.tar.xz | tar -C / -Jxp
# Set s6-overlay behaviours via environment variables
# (see https://github.com/just-containers/s6-overlay#customizing-s6-overlay-behaviour)
ENV S6_KEEP_ENV=1
ENV S6_CMD_WAIT_FOR_SERVICES_MAXTIME=15000

# Add tailscaled and tailscale definitions for s6-overlay
COPY s6-rc.d /etc/s6-overlay/s6-rc.d

# Combine s6-overlay init script and micromamba entrypoint script as global entrypoint
ENTRYPOINT ["/init", "/usr/local/bin/_entrypoint.sh"]

USER ${MAMBA_USER}

# Write MAMBA_EXE and MAMBA_ROOT_PREFIX environment variables to beginning of .bashrc
# Also ENV_NAME for micromamba environment to activate (defaults to 'base')
# (see https://micromamba-docker.readthedocs.io/en/latest/advanced_usage.html#multiple-environments)
# so that micromamba environment activation works when logging in via tailscale ssh
# (see sed syntax for '1i\' command here: https://www.gnu.org/software/sed/manual/sed.html#Other-Commands)
RUN sed -i -f - ${HOME}/.bashrc <<EOF
1i\
export MAMBA_EXE=${MAMBA_EXE}\\
export MAMBA_ROOT_PREFIX=${MAMBA_ROOT_PREFIX}\\
export ENV_NAME=base\n
EOF
